﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_Industriales
{
    public partial class ucNivelTanque : UserControl
    {
        public ucNivelTanque()
        {
            InitializeComponent();
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            tmrLlenar.Enabled = true;
            tmrVaciar.Enabled = false;
        }

        private void btnVaciar_Click(object sender, EventArgs e)
        {
            tmrLlenar.Enabled = false;
            tmrVaciar.Enabled = true;
        }

        /* OPERADORES ARITMÉTICOS
         * Suma:     A = B + C;     incremento en 1:   A++;
         * Resta:    A = B - C;     decremento en 1:   A--;
         * Producto: A = B * C;
         * Cociente: A = B / C;
         * Residuo:  A = B % C;
         */

        private void tmrLlenar_Tick(object sender, EventArgs e)
        {
            if (panelLiquido.Height < panelInterior.Height)
            {
                panelLiquido.Height++;
            }
            else
            {
                tmrLlenar.Enabled = false;
            }
        }

        private void tmrVaciar_Tick(object sender, EventArgs e)
        {
            if (panelLiquido.Height > 0)
            {
                panelLiquido.Height--;
            }
            else
            {
                tmrVaciar.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tmrLlenar.Enabled = false;
            tmrVaciar.Enabled = false;
        }

        private void tmrVariables_Tick(object sender, EventArgs e)
        {
            if (tmrLlenar.Enabled == true)
            {
                pbxLlenado.BackColor = Color.GreenYellow;
            }
            else
            {
                pbxLlenado.BackColor = Color.FromArgb(64, 0, 0);
            }

            if (tmrVaciar.Enabled)
            {
                pbxVaciado.BackColor = Color.GreenYellow;
            }
            else
            {
                pbxVaciado.BackColor = Color.FromArgb(64, 0, 0);
            }
        }
    }
}
