﻿
namespace Simulaciones_Industriales
{
    partial class ucTemperaturaHorno
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pbxHornoON = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnON = new System.Windows.Forms.Button();
            this.btnOFF = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nudTemperatura = new System.Windows.Forms.NumericUpDown();
            this.panelTermómetro = new System.Windows.Forms.Panel();
            this.PanelMercurio = new System.Windows.Forms.Panel();
            this.tmrON = new System.Windows.Forms.Timer(this.components);
            this.tmrOFF = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).BeginInit();
            this.panelTermómetro.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Simulaciones_Industriales.Properties.Resources.Termómetro_26_180_Celcius;
            this.pictureBox3.Location = new System.Drawing.Point(237, 19);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(139, 404);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // pbxHornoON
            // 
            this.pbxHornoON.Image = global::Simulaciones_Industriales.Properties.Resources.Ahumador_encendido_GIF;
            this.pbxHornoON.Location = new System.Drawing.Point(53, 91);
            this.pbxHornoON.Name = "pbxHornoON";
            this.pbxHornoON.Size = new System.Drawing.Size(139, 129);
            this.pbxHornoON.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxHornoON.TabIndex = 0;
            this.pbxHornoON.TabStop = false;
            this.pbxHornoON.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Simulaciones_Industriales.Properties.Resources.Ahumador_apagado;
            this.pictureBox1.Location = new System.Drawing.Point(53, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(139, 129);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnON
            // 
            this.btnON.Location = new System.Drawing.Point(39, 231);
            this.btnON.Name = "btnON";
            this.btnON.Size = new System.Drawing.Size(75, 23);
            this.btnON.TabIndex = 1;
            this.btnON.Text = "Encender";
            this.btnON.UseVisualStyleBackColor = true;
            this.btnON.Click += new System.EventHandler(this.btnON_Click);
            // 
            // btnOFF
            // 
            this.btnOFF.Location = new System.Drawing.Point(133, 231);
            this.btnOFF.Name = "btnOFF";
            this.btnOFF.Size = new System.Drawing.Size(75, 23);
            this.btnOFF.TabIndex = 2;
            this.btnOFF.Text = "Apagar";
            this.btnOFF.UseVisualStyleBackColor = true;
            this.btnOFF.Click += new System.EventHandler(this.btnOFF_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 273);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Temperatura (°C):";
            // 
            // nudTemperatura
            // 
            this.nudTemperatura.Location = new System.Drawing.Point(132, 271);
            this.nudTemperatura.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.nudTemperatura.Minimum = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.Name = "nudTemperatura";
            this.nudTemperatura.Size = new System.Drawing.Size(70, 20);
            this.nudTemperatura.TabIndex = 4;
            this.nudTemperatura.Value = new decimal(new int[] {
            26,
            0,
            0,
            0});
            this.nudTemperatura.ValueChanged += new System.EventHandler(this.nudTemperatura_ValueChanged);
            // 
            // panelTermómetro
            // 
            this.panelTermómetro.BackColor = System.Drawing.Color.White;
            this.panelTermómetro.Controls.Add(this.PanelMercurio);
            this.panelTermómetro.Location = new System.Drawing.Point(292, 62);
            this.panelTermómetro.Name = "panelTermómetro";
            this.panelTermómetro.Size = new System.Drawing.Size(11, 287);
            this.panelTermómetro.TabIndex = 5;
            // 
            // PanelMercurio
            // 
            this.PanelMercurio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(11)))), ((int)(((byte)(21)))));
            this.PanelMercurio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelMercurio.Location = new System.Drawing.Point(0, 203);
            this.PanelMercurio.Name = "PanelMercurio";
            this.PanelMercurio.Size = new System.Drawing.Size(11, 84);
            this.PanelMercurio.TabIndex = 0;
            // 
            // tmrON
            // 
            this.tmrON.Tick += new System.EventHandler(this.tmrON_Tick);
            // 
            // tmrOFF
            // 
            this.tmrOFF.Tick += new System.EventHandler(this.tmrOFF_Tick);
            // 
            // ucTemperaturaHorno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelTermómetro);
            this.Controls.Add(this.nudTemperatura);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOFF);
            this.Controls.Add(this.btnON);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pbxHornoON);
            this.Controls.Add(this.pictureBox1);
            this.Name = "ucTemperaturaHorno";
            this.Size = new System.Drawing.Size(428, 447);
            this.Load += new System.EventHandler(this.ucTemperaturaHorno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHornoON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperatura)).EndInit();
            this.panelTermómetro.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbxHornoON;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnON;
        private System.Windows.Forms.Button btnOFF;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudTemperatura;
        private System.Windows.Forms.Panel panelTermómetro;
        private System.Windows.Forms.Panel PanelMercurio;
        private System.Windows.Forms.Timer tmrON;
        private System.Windows.Forms.Timer tmrOFF;
    }
}
