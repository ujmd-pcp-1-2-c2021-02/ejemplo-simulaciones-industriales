﻿
namespace Simulaciones_Industriales
{
    partial class ucPuertaAutoclave
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelPuerta = new System.Windows.Forms.Panel();
            this.tmrSubir = new System.Windows.Forms.Timer(this.components);
            this.tmrBajar = new System.Windows.Forms.Timer(this.components);
            this.btnBajar = new System.Windows.Forms.Button();
            this.btnSubir = new System.Windows.Forms.Button();
            this.pbxFrontSide = new System.Windows.Forms.PictureBox();
            this.pbxFrontInside = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFrontSide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFrontInside)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.Controls.Add(this.btnBajar);
            this.panel1.Controls.Add(this.btnSubir);
            this.panel1.Location = new System.Drawing.Point(295, 96);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(77, 147);
            this.panel1.TabIndex = 7;
            // 
            // panelPuerta
            // 
            this.panelPuerta.BackColor = System.Drawing.Color.DarkGray;
            this.panelPuerta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPuerta.Location = new System.Drawing.Point(65, 87);
            this.panelPuerta.Name = "panelPuerta";
            this.panelPuerta.Size = new System.Drawing.Size(175, 165);
            this.panelPuerta.TabIndex = 8;
            // 
            // tmrSubir
            // 
            this.tmrSubir.Tick += new System.EventHandler(this.tmrSubir_Tick);
            // 
            // tmrBajar
            // 
            this.tmrBajar.Tick += new System.EventHandler(this.tmrBajar_Tick);
            // 
            // btnBajar
            // 
            this.btnBajar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBajar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBajar.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBajar.FlatAppearance.BorderSize = 3;
            this.btnBajar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBajar.Image = global::Simulaciones_Industriales.Properties.Resources.down_arrow;
            this.btnBajar.Location = new System.Drawing.Point(14, 81);
            this.btnBajar.Name = "btnBajar";
            this.btnBajar.Size = new System.Drawing.Size(48, 48);
            this.btnBajar.TabIndex = 0;
            this.btnBajar.UseVisualStyleBackColor = false;
            this.btnBajar.Click += new System.EventHandler(this.btnBajar_Click);
            // 
            // btnSubir
            // 
            this.btnSubir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSubir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSubir.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSubir.FlatAppearance.BorderSize = 3;
            this.btnSubir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubir.Image = global::Simulaciones_Industriales.Properties.Resources.up_arrow;
            this.btnSubir.Location = new System.Drawing.Point(14, 16);
            this.btnSubir.Name = "btnSubir";
            this.btnSubir.Size = new System.Drawing.Size(48, 48);
            this.btnSubir.TabIndex = 0;
            this.btnSubir.UseVisualStyleBackColor = false;
            this.btnSubir.Click += new System.EventHandler(this.btnSubir_Click);
            // 
            // pbxFrontSide
            // 
            this.pbxFrontSide.BackColor = System.Drawing.Color.Transparent;
            this.pbxFrontSide.Image = global::Simulaciones_Industriales.Properties.Resources.Autoclave___Front_Side;
            this.pbxFrontSide.Location = new System.Drawing.Point(15, 16);
            this.pbxFrontSide.Name = "pbxFrontSide";
            this.pbxFrontSide.Size = new System.Drawing.Size(274, 466);
            this.pbxFrontSide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxFrontSide.TabIndex = 5;
            this.pbxFrontSide.TabStop = false;
            // 
            // pbxFrontInside
            // 
            this.pbxFrontInside.BackColor = System.Drawing.Color.Transparent;
            this.pbxFrontInside.Location = new System.Drawing.Point(15, 16);
            this.pbxFrontInside.Name = "pbxFrontInside";
            this.pbxFrontInside.Size = new System.Drawing.Size(274, 466);
            this.pbxFrontInside.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxFrontInside.TabIndex = 6;
            this.pbxFrontInside.TabStop = false;
            // 
            // ucPuertaAutoclave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelPuerta);
            this.Controls.Add(this.pbxFrontSide);
            this.Controls.Add(this.pbxFrontInside);
            this.Name = "ucPuertaAutoclave";
            this.Size = new System.Drawing.Size(386, 501);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxFrontSide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxFrontInside)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBajar;
        private System.Windows.Forms.Button btnSubir;
        private System.Windows.Forms.Panel panelPuerta;
        private System.Windows.Forms.PictureBox pbxFrontSide;
        private System.Windows.Forms.PictureBox pbxFrontInside;
        private System.Windows.Forms.Timer tmrSubir;
        private System.Windows.Forms.Timer tmrBajar;
    }
}
