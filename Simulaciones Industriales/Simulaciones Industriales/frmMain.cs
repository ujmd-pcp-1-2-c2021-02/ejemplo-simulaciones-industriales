﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_Industriales
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void rbtnInicio_CheckedChanged(object sender, EventArgs e)
        {
            panelInicio.Visible = rbtnInicio.Checked;
        }

        private void rbtnPuertaAutoclave_CheckedChanged(object sender, EventArgs e)
        {
            // Forma larga de cambio de visibilidad del UserControl
            if (rbtnPuertaAutoclave.Checked)
            {
                ucPuertaAutoclave1.Visible = true;
            }
            else
            {
                ucPuertaAutoclave1.Visible = false;
            }
        }

        private void rbtnNivelTanque_CheckedChanged(object sender, EventArgs e)
        {
            // Forma corta con asignación directa en la propiedad visible del UC
            ucNivelTanque1.Visible = rbtnNivelTanque.Checked;
        }

        private void rbtnTemperaturaHorno_CheckedChanged(object sender, EventArgs e)
        {
            ucTemperaturaHorno1.Visible = rbtnTemperaturaHorno.Checked;
        }

        private void btnPuertaAutoclave_Click(object sender, EventArgs e)
        {
            rbtnPuertaAutoclave.Checked = true;
        }

        private void btnNivelTanque_Click(object sender, EventArgs e)
        {
            rbtnNivelTanque.Checked = true;
        }

        private void btnTemperaturaHorno_Click(object sender, EventArgs e)
        {
            rbtnTemperaturaHorno.Checked = true;
        }
    }
}
