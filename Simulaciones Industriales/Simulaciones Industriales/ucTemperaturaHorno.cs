﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_Industriales
{
    public partial class ucTemperaturaHorno : UserControl
    {
        public ucTemperaturaHorno()
        {
            InitializeComponent();
        }

        private void ucTemperaturaHorno_Load(object sender, EventArgs e)
        {
            PanelMercurio.Height = (int)(nudTemperatura.Value - 26) * panelTermómetro.Height / (180 - 26);
        }

        private void nudTemperatura_ValueChanged(object sender, EventArgs e)
        {
            PanelMercurio.Height = (int)(nudTemperatura.Value - 26) * panelTermómetro.Height / (180 - 26);
        }

        private void btnON_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = true;
            tmrOFF.Enabled = false;
            pbxHornoON.Visible = true;
        }

        private void btnOFF_Click(object sender, EventArgs e)
        {
            tmrON.Enabled = false;
            tmrOFF.Enabled = true;
            pbxHornoON.Visible = false;
        }

        private void tmrON_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value < 180)
            {
                nudTemperatura.Value++;
            }
        }

        private void tmrOFF_Tick(object sender, EventArgs e)
        {
            if (nudTemperatura.Value > 26)
            {
                nudTemperatura.Value--;
            }
        }
    }
}
