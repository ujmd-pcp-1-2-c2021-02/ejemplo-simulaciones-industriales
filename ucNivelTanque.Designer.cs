﻿
namespace Simulaciones_Industriales
{
    partial class ucNivelTanque
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelExterior = new System.Windows.Forms.Panel();
            this.panelInterior = new System.Windows.Forms.Panel();
            this.panelLiquido = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pbxVaciado = new System.Windows.Forms.PictureBox();
            this.pbxLlenado = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnVaciar = new System.Windows.Forms.Button();
            this.btnLlenar = new System.Windows.Forms.Button();
            this.tmrLlenar = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciar = new System.Windows.Forms.Timer(this.components);
            this.tmrVariables = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelExterior.SuspendLayout();
            this.panelInterior.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxVaciado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLlenado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelExterior
            // 
            this.panelExterior.BackColor = System.Drawing.Color.Gray;
            this.panelExterior.Controls.Add(this.panelInterior);
            this.panelExterior.Location = new System.Drawing.Point(190, 138);
            this.panelExterior.Name = "panelExterior";
            this.panelExterior.Padding = new System.Windows.Forms.Padding(4);
            this.panelExterior.Size = new System.Drawing.Size(30, 208);
            this.panelExterior.TabIndex = 4;
            // 
            // panelInterior
            // 
            this.panelInterior.BackColor = System.Drawing.Color.White;
            this.panelInterior.Controls.Add(this.panelLiquido);
            this.panelInterior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInterior.Location = new System.Drawing.Point(4, 4);
            this.panelInterior.Name = "panelInterior";
            this.panelInterior.Size = new System.Drawing.Size(22, 200);
            this.panelInterior.TabIndex = 0;
            // 
            // panelLiquido
            // 
            this.panelLiquido.BackColor = System.Drawing.Color.Blue;
            this.panelLiquido.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLiquido.Location = new System.Drawing.Point(0, 112);
            this.panelLiquido.Name = "panelLiquido";
            this.panelLiquido.Size = new System.Drawing.Size(22, 88);
            this.panelLiquido.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gray;
            this.panel3.Controls.Add(this.pbxVaciado);
            this.panel3.Controls.Add(this.pbxLlenado);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.btnVaciar);
            this.panel3.Controls.Add(this.btnLlenar);
            this.panel3.Location = new System.Drawing.Point(20, 169);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(123, 131);
            this.panel3.TabIndex = 5;
            // 
            // pbxVaciado
            // 
            this.pbxVaciado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbxVaciado.Location = new System.Drawing.Point(95, 52);
            this.pbxVaciado.Name = "pbxVaciado";
            this.pbxVaciado.Size = new System.Drawing.Size(22, 21);
            this.pbxVaciado.TabIndex = 1;
            this.pbxVaciado.TabStop = false;
            // 
            // pbxLlenado
            // 
            this.pbxLlenado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.pbxLlenado.Location = new System.Drawing.Point(95, 16);
            this.pbxLlenado.Name = "pbxLlenado";
            this.pbxLlenado.Size = new System.Drawing.Size(22, 21);
            this.pbxLlenado.TabIndex = 1;
            this.pbxLlenado.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 89);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Detener";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnVaciar
            // 
            this.btnVaciar.Location = new System.Drawing.Point(14, 52);
            this.btnVaciar.Name = "btnVaciar";
            this.btnVaciar.Size = new System.Drawing.Size(75, 23);
            this.btnVaciar.TabIndex = 0;
            this.btnVaciar.Text = "Vaciar";
            this.btnVaciar.UseVisualStyleBackColor = true;
            this.btnVaciar.Click += new System.EventHandler(this.btnVaciar_Click);
            // 
            // btnLlenar
            // 
            this.btnLlenar.Location = new System.Drawing.Point(14, 15);
            this.btnLlenar.Name = "btnLlenar";
            this.btnLlenar.Size = new System.Drawing.Size(75, 23);
            this.btnLlenar.TabIndex = 0;
            this.btnLlenar.Text = "Llenar";
            this.btnLlenar.UseVisualStyleBackColor = true;
            this.btnLlenar.Click += new System.EventHandler(this.btnLlenar_Click);
            // 
            // tmrLlenar
            // 
            this.tmrLlenar.Interval = 50;
            this.tmrLlenar.Tick += new System.EventHandler(this.tmrLlenar_Tick);
            // 
            // tmrVaciar
            // 
            this.tmrVaciar.Interval = 50;
            this.tmrVaciar.Tick += new System.EventHandler(this.tmrVaciar_Tick);
            // 
            // tmrVariables
            // 
            this.tmrVariables.Enabled = true;
            this.tmrVariables.Tick += new System.EventHandler(this.tmrVariables_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Simulaciones_Industriales.Properties.Resources.Recurso_1tank;
            this.pictureBox1.Location = new System.Drawing.Point(166, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(384, 361);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // ucNivelTanque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelExterior);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel3);
            this.Name = "ucNivelTanque";
            this.Size = new System.Drawing.Size(568, 406);
            this.panelExterior.ResumeLayout(false);
            this.panelInterior.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxVaciado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLlenado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelExterior;
        private System.Windows.Forms.Panel panelInterior;
        private System.Windows.Forms.Panel panelLiquido;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbxVaciado;
        private System.Windows.Forms.PictureBox pbxLlenado;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnVaciar;
        private System.Windows.Forms.Button btnLlenar;
        private System.Windows.Forms.Timer tmrLlenar;
        private System.Windows.Forms.Timer tmrVaciar;
        private System.Windows.Forms.Timer tmrVariables;
    }
}
